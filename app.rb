require 'sinatra'
require 'sinatra/base'
require "sinatra/activerecord"

load "app/models/content.rb"

register Sinatra::ActiveRecordExtension

before do
  content_type :json
end

get '/' do
  "Hello World!"
end

get '/contents' do
  @contents = Content.all
  @contents.to_json
end

get '/content/:id' do
  @content = Content.find(params[:id])
  @content.to_json
end

post '/content' do
  if params[:content][:id]
    status 400
    return 'You can not chose content id'
  end
  @content = Content.new(params[:content])
  if @content.save
    @content.to_json
  else
    status 400
    @content.errors.full_messages.to_json
  end
end

put '/content/:id' do
  if params[:id] != params[:content][:id]
    status 400
    return 'You can not change content id'
  end
  @content = Content.find(params[:id])
  if @content.update(params[:content])
    @content.to_json
  else
    status 400
    @content.errors.full_message.to_json
  end
end

delete '/content/:id' do
  @content = Content.find(params[:id])
  @content.destroy
end
