# Scire project

## Contributions

### General rules

- Please use MR, and don't review your own code ;)
- English please!
- Use the wiki! Try to improve the documentation if necessary. 
The documentation is not a waste of time, but a saving of time for the rest of the contributors.

### Technical part

#### Getting started

```bash
# Install dependencies
bundle install

# Create database
bundle exec rake db:create

# Migrate database
bundle exec rake db:migrate

# Start server
bundle exec rerun "ruby app.rb"
```

#### Unit tests

```bash
bundle exec rake db:test:prepare
bundle exec rake db:fixtures:load RACK_ENV=test
bundle exec ruby test/app_test.rb 
```




