class Content < ActiveRecord::Base
  validates_presence_of :title, :title_lang, :link, :publication_date, :tags
end
