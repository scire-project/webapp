ENV['RACK_ENV'] = 'test'

require './app'
require 'test/unit'
require 'rack/test'

class ContentTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  def test_get_contents
    get '/contents'
    assert last_response.ok?
    assert_match /first/, last_response.body
  end

  def test_get_a_content
    get '/content/1'
    assert last_response.ok?
    assert_match /first/, last_response.body
  end

  def test_get_nonexisting_content
    assert_raises(ActiveRecord::RecordNotFound) do
      get '/content/200'
    end
  end

  def test_add_valid_content
    params = {content: {
        title: 'Wow',
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
        tags: 'culture'
    }}
    post '/content', params
    assert last_response.ok?
  end

  def test_add_missing_title_content
    params = {content: {
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
        tags: 'culture'
    }}
    post '/content', params
    assert !last_response.ok?
  end

  def test_add_missing_lang_content
    params = {content: {
        title: 'Wow',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
        tags: 'culture'    }}
    post '/content', params
    assert !last_response.ok?
  end

  def test_add_missing_link_content
    params = {content: {
        title: 'Wow',
        title_lang: 'fr',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
        tags: 'culture'
    }}
    post '/content', params
    assert !last_response.ok?
  end

  def test_add_missing_date_content
    params = {content: {
        title: 'Wow',
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        audio_lang: 'fr',
        tags: 'culture'
    }}
    post '/content', params
    assert !last_response.ok?
  end

  def test_add_missing_tags_content
    params = {content: {
        title: 'Wow',
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
    }}
    post '/content', params
    assert !last_response.ok?
  end

  def test_add_content_id
    params = {content: {
        id: 500,
        title: 'Wow',
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
    }}
    post '/content', params
    assert !last_response.ok?
  end

  def test_edit_existing_content
    params = {content: {
        id: 1,
        title: 'first',
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
    }}
    put '/content/1', params
    assert last_response.ok?
  end

  def test_edit_content_id
    params = {content: {
        id: 6,
        title: 'first',
        title_lang: 'fr',
        link: 'http://ddg.gg/',
        publication_date: '15/10/1996',
        audio_lang: 'fr',
    }}
    put '/content/1', params
    assert !last_response.ok?
  end

  def test_delete_existing_content
    delete '/content/2'
    assert last_response.ok?
  end

  def test_del_nonexisting_content
    assert_raises(ActiveRecord::RecordNotFound) do
      delete '/content/24678'
    end
  end
end